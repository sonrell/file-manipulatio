package menu;


import java.util.List;
import java.util.Scanner;

public class Menu {
    int menuRange;
    List<String>  menuText;

    // Constructor, setters, and getters.
    public Menu(int menuRange, List<String> menuText) {
        this.menuRange = menuRange;
        this.menuText = menuText;
    }


    public void setMenuRange(int menuRange) {
        this.menuRange = menuRange;
    }

    public List<String> getMenuText() {
        return menuText;
    }

    public void setMenuText(List<String> menuText) {
        this.menuText = menuText;
    }




    public int promptMenu(){
        Scanner inputScanner= new Scanner(System.in);
        boolean correctInput=false;
        int inputNum=0;

        // loop checks for valid input
        while(!correctInput){                                               // Repeats when wrong input is given
            System.out.println();

            for (int ii = 0; ii < this.menuRange; ii++) {                   // Loops through menuRange
                System.out.printf("%d: %s %n",ii+1,menuText.get(ii));       // Prints menu (1: something something)
            }
            System.out.printf("Please choose an alternative: %n");
            try{
                inputNum=inputScanner.nextInt();
                if (inputNum>0 && inputNum <= this.menuRange) {             // If integer input inside range
                    correctInput = true;
                }else{                                                      // Integer outside range
                    correctInput = false;
                    System.out.println("You have entered an integer outside the range of the menu please try again: ");
                }
            }catch (Exception e){
                System.out.println("You have entered something else than an integer, please try again: ");
            }
            inputScanner.nextLine();
        }
        return inputNum;
    }
}
