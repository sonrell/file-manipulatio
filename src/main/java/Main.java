import logging.*;
import menu.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import static fileManipulateFuncs.FileLister.*;
import static fileManipulateFuncs.TextManipulator.*;




public class Main {


    static int mainChoose;
    static int fileChoose;
    static int txtChoose;
    static int extChoose;

    public static void runMenu(){

        // Initializing ranges and lists used to create menus  main=main menu, file =file menu, txt= dracula.txt menu
        List<String> listMainTex    = new ArrayList<>();
        List<String> listFileTex    = new ArrayList<>();
        List<String> listTxtTex     = new ArrayList<>();



        //Adding options to the menus
        listMainTex.add("List files in \\resources folder ");
        listMainTex.add("Get info about the Dracula.txt file ");
        listMainTex.add("Exit Program ");

        listFileTex.add("List all files in \\resources folder ");
        listFileTex.add("List all files in \\resources folder with a specific extension ");
        listFileTex.add("Return to main menu ");
        listFileTex.add("Exit Program ");

        listTxtTex.add("Print the name of the file out ");
        listTxtTex.add("Print the size of the file ");
        listTxtTex.add("Print the amount of the line the file has ");
        listTxtTex.add("Check if a word in the file exists ");
        listTxtTex.add("Check the amount of times a word occurs in the file "); //search ignore case
        listTxtTex.add("Return to main menu ");
        listTxtTex.add("Exit Program ");




        //Setting range of menus
        int mainRange   = listMainTex.size();
        int fileRange   = listFileTex.size();
        int txtRange    = listTxtTex.size();


        //Creating menus
        Menu mainMenu= new Menu(mainRange,listMainTex);
        Menu fileMenu= new Menu(fileRange,listFileTex);
        Menu txtMenu= new Menu(txtRange,listTxtTex);
        Menu extMenu= new Menu(0, Collections.emptyList()) ;



       // Initializing variables for the menu-loop
        boolean exitProg=false;
        boolean returnToMain;
        String  logString;
        String  dirResources="src/main/resources";
        String  draculaPath="src/main/resources/Dracula.txt";
        String logPath="log\\log.txt";
        long startTime;
        Logger logger= new Logger(logPath);

        //Loop for prompting the main menu and running all associated methods

        //mainMenu : main menu
        //fileMenu : file listing menu: list all files and list by extension
        //txtMenu  : all dracula.txt methods

        /* All methods are logged, first getting the start time with a log function and then pass it as argument,
        together with logString which is a string returned by the methods and also printed to console.
        */

        while(!exitProg){
            mainChoose= mainMenu.promptMenu();
            returnToMain=false;
        // while loop inside a sub-menu:
            while(!returnToMain && !exitProg) {
                switch (mainChoose) {
                    case 1 -> {
                        fileChoose = fileMenu.promptMenu();                             // Prompts List all files etc..
                        switch (fileChoose) {

                            case 1 -> {                                                 //List all files
                                startTime = logger.getStartTime();
                                logString = listAllFiles(dirResources);
                                System.out.println(logString);
                                logger.writeToLogfile(logString, startTime);
                            }

                            case 2 -> {                                                 //List all files by extension
                                List allExtensions = getAllExtensions(dirResources);    //Gets all options of extensions
                                extMenu.setMenuText(allExtensions);                     //Creating a menu of options.
                                extMenu.setMenuRange(allExtensions.size());
                                extChoose = extMenu.promptMenu();
                                startTime = logger.getStartTime();
                                logString = listFilesByExtension
                                        (dirResources, allExtensions.get(extChoose - 1).toString());
                                System.out.println(logString);
                                logger.writeToLogfile(logString, startTime);
                            }

                            case 3 -> returnToMain = true;                              // Return to main

                            case 4 -> exitProg = true;                                  // Exit program
                        }
                    }
                    case 2 -> {
                        txtChoose = txtMenu.promptMenu();                               // Prompts dracula.txt options
                        switch (txtChoose) {

                            case 1 -> {                                                 // Prints file name
                                startTime = logger.getStartTime();
                                logString = getFilename(draculaPath);
                                System.out.println(logString);
                                logger.writeToLogfile(logString, startTime);
                            }

                            case 2 -> {                                                 // Prints file size
                                startTime = logger.getStartTime();
                                logString = getFilesize(draculaPath);
                                System.out.println(logString);
                                logger.writeToLogfile(logString, startTime);
                            }

                            case 3 -> {                                                 // Prints number of lines
                                startTime = logger.getStartTime();
                                logString = getNumberOfLines(draculaPath);
                                System.out.println(logString);
                                logger.writeToLogfile(logString, startTime);
                            }

                            case 4 -> {                                                 // Check if it contains a word
                                startTime = logger.getStartTime();
                                logString = doesContainWord(draculaPath);
                                System.out.println(logString);
                                logger.writeToLogfile(logString, startTime);
                            }
                                                                                        //Returns nr of word occurrences
                            case 5 -> {
                                startTime = logger.getStartTime();
                                logString = amountContainWord(draculaPath);
                                System.out.println(logString);
                                logger.writeToLogfile(logString, startTime);
                            }

                            case 6 -> returnToMain = true;                              // Return to main menu

                            case 7 -> exitProg = true;                                  // Exit program
                        }
                    }

                    case 3 -> exitProg = true;                                          // case 3 from Main to exit
                }
            }




        }

    }

    public static void main(String[] args) {
        runMenu();
    }
}
