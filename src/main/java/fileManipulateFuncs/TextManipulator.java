package fileManipulateFuncs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;

public class TextManipulator {

    public static String getFilename(String dir){

        try{
            File f = new File(dir);                                             //file where dir points
            return "The file name is \""+ f.getName() +"\"";
        }catch(Exception e){
            System.err.println(e.getMessage());
            return e.getMessage();
        }

    }

    public static String getFilesize(String dir){

        try{
            double sizeInKb;
            File f = new File(dir);
            sizeInKb=f.length()/1024;
            return "The file size is " + sizeInKb +" kB";

        }catch(Exception e){
            System.err.println(e.getMessage());
            return e.getMessage();
        }

    }
    public static String getNumberOfLines(String dir) {

        int linesTheFileHas = 0;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(dir))){
            String line;
            while ((line = bufferedReader.readLine()) != null) {                            // process the line
                linesTheFileHas++;                                                          // counts lines
            }
        }catch (Exception ex) {
                System.out.println(ex.getMessage());
        }

        return "The file has " + String.valueOf(linesTheFileHas) + " number of lines" ;

        }

    public static String doesContainWord(String dir){

        String wordToContain;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(dir))){
            String line;
            System.out.print("Please enter a word: ");
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
            wordToContain = inputReader.readLine();                                             //word to search for

            while ((line = bufferedReader.readLine()) != null) {                                // process the line
                if (line.toLowerCase().contains(wordToContain.toLowerCase())){                  //search ignore case
                    return "The file contains \"" + wordToContain + "\"" ;                      //the file contains
                }
            }                                                                                   //while loop finished:
            return "The file does not contain \"" + wordToContain + "\"";                      // which means not found
        }catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }



    }

    public static String amountContainWord(String dir){

        int amountWordOccurs=0;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(dir))){
            String line;
            String wordToContain;

            System.out.print("Please enter a word: ");
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));   // User input word
            wordToContain = inputReader.readLine();                                              // Word to search for

            while ((line = bufferedReader.readLine()) != null) {
                if (line.toLowerCase().contains(wordToContain.toLowerCase())){                  //if word found
                    amountWordOccurs++;                                                         // increase word found
                }
            }
            if (amountWordOccurs!=1) {                                                         //return time or times?
                return "The word \"" + wordToContain + "\" was found " +
                        amountWordOccurs + " times";
            }else{                                                                              //test for "monkey"
                return "The word \"" + wordToContain + "\" was found " +
                        amountWordOccurs + " time";
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }


}
