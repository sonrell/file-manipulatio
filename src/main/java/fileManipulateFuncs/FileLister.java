package fileManipulateFuncs;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileLister {

    public static String listAllFiles(String dir){

        StringBuilder logTextToRet= new StringBuilder("The following files were found: \"");
        try {

            File f = new File(dir);
            File[] files = f.listFiles();

            // listing all files separated by "," and prints to console
            for (int i = 0; i < Objects.requireNonNull(files).length; i++) {
                if (i!= files.length-1) {                               // if sentence to prevent to end string with ","
                    logTextToRet.append(files[i].getName()).append("\", \"");
                }else{
                    logTextToRet.append(files[i].getName()).append("\"");
                }
            }

        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return logTextToRet.toString();


    }
    public static String listFilesByExtension(String dir, String ext){     // takes the directory and a specific ext

        StringBuilder logTextToRet= new StringBuilder(String.format("The following files were found with extension \"%s\": ", ext));
        try {
            File f = new File(dir);
            //Filter to accept only extensions passed as arg
            FilenameFilter filter = (f1, name) -> name.endsWith(ext);

            File[] files = f.listFiles(filter);                         //List of all files with extensions=ext
            for (int i = 0; i < Objects.requireNonNull(files).length; i++) {

                if (i!= files.length-1) {                               // if sentence to prevent to end string with ","
                    logTextToRet.append(files[i].getName()).append("\", \"");
                }else{
                    logTextToRet.append(files[i].getName()).append("\"");
                }
            }

        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return logTextToRet.toString();


    }
    public static List getAllExtensions(String dir){

            List<String> listOfExtensions  = new ArrayList<>();

        try {
            File f = new File(dir);                                         // directory  f.ex /resources
            File[] files = f.listFiles();                                   // list of all files in dir
            String[] splitFileString;                                       // Used to distinguish by extension
            List<String> alreadyListed  = new ArrayList<>();                // Used to already registered extensions
            String fileName;

            // Display the names of the files
            if (files == null) throw new AssertionError();
            for (File file : files) {
                fileName = file.getName();                                //filename for every in dir
                splitFileString = fileName.split("\\.");               //split by all "."

                /* the last element in splitfileString will contain the extension, if sentence checks if not
                alreadylisted contains it, if not it adds it to the list of extension and to alreadylisted.
                 */
                if (!alreadyListed.contains(splitFileString[splitFileString.length - 1])) {
                    alreadyListed.add(splitFileString[splitFileString.length - 1]);
                    listOfExtensions.add(splitFileString[splitFileString.length - 1]);
                }
            }
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return listOfExtensions;
    }
}
