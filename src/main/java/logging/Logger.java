package logging;


import java.io.File;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class Logger {


    public Logger(String filePath) {

        try{
            this.filePath = filePath;
            File file = new File(filePath);             // Creates a new file and dir
            file.getParentFile().mkdir();
            file.createNewFile();


        }catch(Exception e){
            System.err.println(e.getMessage());
        }

    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    String filePath;

    public long getStartTime(){
        return System.currentTimeMillis();
    }
    public long getMethodTime(long startTime){
        return System.currentTimeMillis() - startTime;
    }
    public void writeToLogfile(String messageToLog, long startTime){

        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            String dateAndTime= dtf.format(now) ;                                       //Current date and time

            FileWriter myWriter = new FileWriter(this.getFilePath(),true);      //Writer to log
            String fullLogString=                                                      //String to write
                    String.format("%s: %s and it took %s ms to execute. \n"
                            ,dateAndTime, messageToLog, getMethodTime(startTime));
            myWriter.write(fullLogString);                                              //Writes the String
            myWriter.close();                                                           //Closes writer

        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }




}


